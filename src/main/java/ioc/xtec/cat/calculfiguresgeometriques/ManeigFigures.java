package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

public class ManeigFigures {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Seleccioneu la figura geomètrica:");
        System.out.println("1. Quadrat");
        System.out.println("2. Cercle");
        System.out.println("3. Triangle");
        /*System.out.println("4. Rectangle");
        System.out.println("5. Pentàgon");
        System.out.println("6. Hexàgon");
        System.out.println("7. Circumferència");*/
        int opcio = scanner.nextInt();
        
        FiguraGeometrica figura = null;
        
        switch (opcio) {
            case 1:
                figura = new Quadrat();
                break;
            case 2:
                figura = new Cercle();
                break;
            /*case 3:
                figura = new Triangle();
                break;
            /*case 4:
                figura = new Rectangle();
                break;
            case 5:
                figura = new Pentagon();
                break;
            case 6:
                figura = new Hexagon();
                break;
            case 7:
                figura = new Circumferencia();
                break;*/
            default:
                System.out.println("Opció no vàlida.");
                return;
        }
        
        System.out.println("Àrea: " + figura.calcularArea());
        System.out.println("Perímetre: " + figura.calcularPerimetre());
    
    }
    
}
