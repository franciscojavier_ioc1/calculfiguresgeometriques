package ioc.xtec.cat.calculfiguresgeometriques;

public interface FiguraGeometrica {
    double calcularArea();
    double calcularPerimetre();
}
