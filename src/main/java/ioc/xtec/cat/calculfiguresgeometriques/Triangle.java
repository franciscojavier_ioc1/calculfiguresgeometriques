/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
public class Triangle implements FiguraGeometrica{
    
    private int costat1;
    private int costat2;
    private int base;
    private int altura;
    
    public Triangle(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu un costat del triangle: ");
        this.costat1 = scanner.nextInt();
        
        System.out.println("Introduïu l'altre costat del triangle: ");
        this.costat2 = scanner.nextInt();
        
        System.out.println("Introduïu la base del triangle: ");
        this.base = scanner.nextInt();
        
        System.out.println("Introduïu l'altura del triangle: ");
        this.base = scanner.nextInt();
        
    }

    @Override
    public double calcularArea() {
        return (this.base * this.altura) / 2;
    }

    @Override
    public double calcularPerimetre() {
        return this.costat1 + this.costat2 + this.base;
    }
    
}
