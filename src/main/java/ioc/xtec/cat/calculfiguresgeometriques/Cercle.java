/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author Xavi Sanz <franciscojavier.s.r.91@gmail.com>
 */
public class Cercle implements FiguraGeometrica{

    private double radi;

    public Cercle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu el radi: ");
        this.radi = scanner.nextDouble();;
    }

    @Override
    public double calcularArea() {
        return Math.PI * Math.pow(radi, 2);
    }

    @Override
    public double calcularPerimetre() {
        return 2 * Math.PI * radi;
    }
}